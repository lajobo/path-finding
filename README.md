# Path finding

This project aims to visualize the path finding algorithm A* with the Manhattan heuristic.

A brief explanation and documentation of the algorithm and heuristic can be found [here](http://theory.stanford.edu/~amitp/GameProgramming/AStarComparison.html).

## Quick start

```bash
docker-compose up
```
or
```bash
npm run start
```

Then open [http://localhost:3000/](http://localhost:3000/) to see the application.

## Usage

At the top, you see the settings for the **grid size**. You can enter any grid size > 0, but keep in mind the bigger the grid, the more performance it will take.

Below the grid size you see the controls, lets skip those for the moment.

At the bottom you see the grid itself. You can click each field to set its state.

* The **black** fields symbolize walls which can't be passed by the algorithm

* The **green** field is where the algorithm starts

* The **red** field is the target of the algorithm

By clicking a field once it will become a wall. After clicking it again, it will become a start or target field, depending if there is already a start or target field. One more click and it will be an empty field again which can be used by the algorithm.

After you have build a bunch of walls, a start and a target, you can click the play button above in the control panel. You will see how the algorithm starts to calculate. The fields around the start will color as well now.

* The **blue** field symbolizes the current field which is being checked by the algorithm

* The **yellow** fields around the blue fields are the fields, that the algorithm knows how to reach from the start but weren't completely checked yet

* The **purple** fields are the result of the blue field. After all accessible neighbors where checked, purple will symbolize that this field is done

After the algorithm has found the fastest path from the start to the target, it will color the fields in reversed order from target to start in **orange**.

If you want to see what is happening in detail, you can use the **control panel**.

* The play button in the center will automatically step through the algorithm until it finishes. After it has been clicked, it will switch to a pause button to stop it again

* The most outer buttons will jump to the first and last step, respectively

* The buttons between the most outer and the play/pause button will go one step forward and backward at a time, respectively. That way you can follow the algorithm easily

As soon as you click any field while the path finding is going on, it will reset so that you can start another run with your changes.

If the target can't be reached, the algorithm will check all accessible fields to be sure that there is no path to the target but after all, it won't color an orange path. 

## Design

The design was kept as simple as possible, since the focus of this project has been the path finding itself. It works best on mobile devices/emulators.

## Extensibility

The whole project is written in a way, that it could easily support more then just the [A* algorithm](http://theory.stanford.edu/~amitp/GameProgramming/AStarComparison.html) and the [Manhattan heuristic](http://theory.stanford.edu/~amitp/GameProgramming/Heuristics.html#manhattan-distance). But the aim was to visualize the steps this algorithm is doing, therefore the project is considered done and won't be extended. 

#### Author

* [Lars Bomnüter](mailto:larsbomnueter@web.de)

---

MIT License
