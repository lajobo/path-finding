module.exports = {
    devtool: "source-map",
    plugins: [
        {
            name: 'typescript',
            options: {
                useBabel: false,
                tsLoader: {
                    transpileOnly: true,
                    experimentalWatchApi: true,
                },
                forkTsChecker: {
                    eslint: {
                        files: ['**/*.ts', '**/*.tsx'],
                    }
                }
            }
        }
    ]
};
