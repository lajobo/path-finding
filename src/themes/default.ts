import { Theme } from "../types/themes";

export const defaultTheme: Theme = {
    space: [0, 4, 8, 16, 32, 64, 128, 256],
    fontSizes: [8, 12, 16, 20, 24, 28, 32, 44],
    lineHeights: ["10px", "14px", "18px", "22px", "28px", "32px", "36px", "48px"],
    breakpoints: ["576px", "768px", "992px", "1200px"],
    colors: {
        grey: {
            dark: "#333333",
            medium: "#aaaaaa",
            bright: "#cccccc"
        },
        error: "#ea5151",
        fields: {
            empty: "#ffffff",
            wall: "#333333",
            start: "#b6fe7e",
            target: "#ff8282",
            current: "#28d6d8",
            open: "#f8f8b9",
            closed: "#d1abff",
            path: "#fa9c5f"
        }
    },
    fonts: {
        default: "Source Sans Pro, sans-serif",
        headings: "Lato, sans-serif"
    }
};
