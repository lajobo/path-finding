import React, { useState } from "react";
import { Box } from "../ui/box";
import { Settings, SettingsConfiguration } from "../settings/settings";
import { Grid } from "../grid/grid";
import { PathfindingControls } from "../control/pathfinding";
import { PathFinding } from "../../types/pathFinding";
import { useAStar } from "../../hooks/useAStar";
import { InvalidFieldsMessage } from "../info/invalidFields";

export const Overview: React.FC = () => {
    const [configuration, setConfiguration] = useState<SettingsConfiguration>();
    const pathFinding: PathFinding = useAStar(Number(configuration?.gridHeight || 0), Number(configuration?.gridWidth || 0));

    return (
        <Box width={"100%"} maxWidth={"1200px"} mx={"auto"} p={[3, 3, 4, 4]}>
            <Settings
                onConfigurationChange={
                    (newConfiguration: SettingsConfiguration, isValid: boolean) => {
                        // skip the configuration update and stick with the old configuration if the updated one is invalid
                        if (isValid) {
                            setConfiguration(newConfiguration);
                        }
                    }
                }
            />
            {configuration && pathFinding.fields && (
                <>
                    <PathfindingControls
                        isPlaying={pathFinding.isAutoActive}
                        onFirstClick={pathFinding.onFirstClick}
                        onPrevClick={pathFinding.onPrevClick}
                        onPlayClick={pathFinding.onPlayClick}
                        onPauseClick={pathFinding.onPauseClick}
                        onNextClick={pathFinding.onNextClick}
                        onLastClick={pathFinding.onLastClick}
                        hasNextStep={pathFinding.hasNextStep}
                        hasPrevStep={pathFinding.hasPrevStep}
                        mt={3}
                    />
                    {pathFinding.fieldsInvalid && (<InvalidFieldsMessage mt={1} />)}
                    <Grid
                        fields={pathFinding.fields}
                        mt={3}
                        onClick={pathFinding.onFieldClick}
                    />
                </>
            )}
        </Box>
    );
};
