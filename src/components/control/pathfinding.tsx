import React, { useContext } from "react";
import { SpaceProps } from "styled-system";
import { ThemeContext } from "styled-components";
import { Box } from "../ui/box";
import { FirstIcon } from "../icon/first";
import { PrevIcon } from "../icon/prev";
import { PlayIcon } from "../icon/play";
import { NextIcon } from "../icon/next";
import { LastIcon } from "../icon/last";
import { PauseIcon } from "../icon/pause";
import { ControlButton } from "../button/control";
import { OnControlClick } from "../../types/pathFinding";
import { Theme } from "../../types/themes";

interface PathfindingControlsProps extends SpaceProps {
    isPlaying: boolean;
    onFirstClick?: OnControlClick;
    onPrevClick?: OnControlClick;
    onPlayClick?: OnControlClick;
    onPauseClick?: OnControlClick;
    onNextClick?: OnControlClick;
    onLastClick?: OnControlClick;
    hasNextStep?: boolean;
    hasPrevStep?: boolean;
}

export const PathfindingControls: React.FC<PathfindingControlsProps> = ({
    isPlaying,
    onFirstClick,
    onPrevClick,
    onPlayClick,
    onPauseClick,
    onNextClick,
    onLastClick,
    hasNextStep,
    hasPrevStep,
    ...boxProps
}: PathfindingControlsProps) => {
    const theme: Theme = useContext(ThemeContext);

    return (
        <Box
            display={"flex"}
            flexDirection={"row"}
            alignItems={"stretch"}
            maxWidth={"500px"}
            mx={"auto"}
            {...boxProps}
        >
            <ControlButton onClick={onFirstClick}>
                <FirstIcon height={"20px"} fill={hasPrevStep ? undefined : theme.colors.grey.bright} />
            </ControlButton>
            <ControlButton onClick={onPrevClick}>
                <PrevIcon height={"20px"} fill={hasPrevStep ? undefined : theme.colors.grey.bright} />
            </ControlButton>
            <ControlButton onClick={isPlaying ? onPauseClick : onPlayClick}>
                {!isPlaying && (<PlayIcon height={"20px"} fill={hasNextStep ? undefined : theme.colors.grey.bright} />)}
                {isPlaying && (<PauseIcon height={"20px"} />)}
            </ControlButton>
            <ControlButton onClick={onNextClick}>
                <NextIcon height={"20px"} fill={hasNextStep ? undefined : theme.colors.grey.bright} />
            </ControlButton>
            <ControlButton onClick={onLastClick}>
                <LastIcon height={"20px"} fill={hasNextStep ? undefined : theme.colors.grey.bright} />
            </ControlButton>
        </Box>
    );
};
