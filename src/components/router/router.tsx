import React from "react";
import { Route, Switch } from "react-router-dom";
import { Overview } from "../overview/overview";

export const Router: React.FC = () => (
    <Switch>
        <Route exact path={"/"} component={Overview} />
    </Switch>
);
