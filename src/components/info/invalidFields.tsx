import React, { useContext } from "react";
import { ThemeContext } from "styled-components";
import { Box, BoxProps } from "../ui/box";
import { Theme } from "../../types/themes";

export const InvalidFieldsMessage: React.FC<BoxProps> = (props: BoxProps) => {
    const theme: Theme = useContext(ThemeContext);
    return (
        <Box display={"block"} color={theme.colors.error} {...props}>
            {"The grid you have built is invalid. You need one start (green) and one target (red) field to start."}
        </Box>
    );
};
