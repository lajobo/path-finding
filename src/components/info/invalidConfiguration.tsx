import React, { useContext } from "react";
import { ThemeContext } from "styled-components";
import { Box, BoxProps } from "../ui/box";
import { Theme } from "../../types/themes";

export const InvalidConfigurationMessage: React.FC<BoxProps> = (props: BoxProps) => {
    const theme: Theme = useContext(ThemeContext);
    return (
        <Box display={"block"} color={theme.colors.error} {...props}>
            {"The grid size is invalid. Please check your input, only positive integers are allowed."}
        </Box>
    );
};
