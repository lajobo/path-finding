import React, { useContext } from "react";
import { ThemeContext } from "styled-components";
import { Box } from "../ui/box";
import { Theme } from "../../types/themes";
import { Text } from "../ui/text";

export const MainHeader: React.FC = () => {
    const theme: Theme = useContext(ThemeContext);
    return (
        <Box
            display={"block"}
            width={"100%"}
            mx={"auto"}
            p={[3, 3, 4, 4]}
            backgroundColor={theme.colors.grey.bright}
        >
            <Text
                as={"h1"}
                fontSize={[3, 3, 4, 4]}
                lineHeight={[3, 3, 4, 4]}
                color={theme.colors.grey.dark}
                fontFamily={theme.fonts.headings}
                fontWeight={"bold"}
            >
                {"PATH FINDING"}
            </Text>
        </Box>
    );
};
