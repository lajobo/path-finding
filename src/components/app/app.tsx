import React from "react";
import { ThemeProvider } from "styled-components";
import { Router } from "../router/router";
import "reset-css";
import { defaultTheme } from "../../themes/default";
import { Box } from "../ui/box";
import { MainHeader } from "../header/main";

export const App: React.FC = () => (
    <ThemeProvider theme={defaultTheme}>
        <Box display={"flex"} height={"100%"} flexDirection={"column"}>
            <MainHeader />
            <Box display={"block"} flexGrow={1}>
                <Router />
            </Box>
        </Box>
    </ThemeProvider>
);
