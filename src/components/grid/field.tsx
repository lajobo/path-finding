import React, { useContext } from "react";
import { ThemeContext } from "styled-components";
import { Box } from "../ui/box";
import { Theme } from "../../types/themes";
import { AlgorithmStates, FieldTypes } from "../../types/fields";

interface GridFieldProps {
    type: FieldTypes;
    algorithmState?: AlgorithmStates;
    size: number;
    onClick?: () => void;
}

export const GridField: React.FC<GridFieldProps> = (props: GridFieldProps) => {
    const theme: Theme = useContext(ThemeContext);

    // start and target field wont get a background color if a algorithmState is set since it would hide the start/target background color.
    // instead its getting a border in those cases.
    const isStartOrTarget: boolean = [FieldTypes.Start, FieldTypes.Target].indexOf(props.type) >= 0;
    const spacialBorderTreatment: boolean = isStartOrTarget && !!props.algorithmState;
    const borderWidth: number = spacialBorderTreatment ? Math.min(Math.round(props.size / 2), 8) : 2;
    const borderColor: string = spacialBorderTreatment ? theme.colors.fields[props.algorithmState] : theme.colors.grey.bright;

    return (
        <Box
            onClick={() => {
                if (props.onClick) {
                    props.onClick();
                }
            }}
            display={"inline-block"}
            width={`${props.size}px`}
            height={`${props.size}px`}
            border={`${borderWidth}px solid ${borderColor}`}
            backgroundColor={theme.colors.fields[isStartOrTarget ? props.type : (props.algorithmState || props.type)]}
        />
    );
};
