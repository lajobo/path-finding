import React, { useContext, useEffect, useRef, useState } from "react";
import { ThemeContext } from "styled-components";
import { SpaceProps } from "styled-system";
import { Box } from "../ui/box";
import { GridField } from "./field";
import { Theme } from "../../types/themes";
import { Fields } from "../../types/fields";

interface GridProps extends SpaceProps {
    fields: Fields;
    onClick?: (row: number, column: number) => void;
}

export const Grid: React.FC<GridProps> = ({
    fields,
    onClick,
    ...boxProps
}: GridProps) => {
    const height: number = fields ? fields.length : 0;
    const width: number = fields && fields[0] ? fields[0].length : 0;
    const theme: Theme = useContext(ThemeContext);
    const containerRef: React.RefObject<HTMLDivElement> = useRef<HTMLDivElement>();
    const [fieldSize, setFieldSize] = useState<number>(0);
    const calculateFieldSize: () => void = () => {
        setFieldSize(containerRef.current.getBoundingClientRect().width / width);
    };

    useEffect(() => {
        if (fieldSize === 0) {
            calculateFieldSize();
        }
    }, [containerRef]);

    useEffect(() => {
        calculateFieldSize();
    }, [width, height]);

    return (
        <Box
            ref={containerRef}
            width={"100%"}
            {...boxProps}
        >
            <Box
                width={fieldSize ? `${fieldSize * width}px` : "0"}
                height={fieldSize ? `${fieldSize * height}px` : "0"}
                margin={"0 auto"}
                display={"flex"}
                flexDirection={"row"}
                flexWrap={"wrap"}
                outline={`2px solid ${theme.colors.grey.bright}`}
            >
                {([...Array(height || 0).keys()].map(
                    (row: number) => (
                        [...Array(width || 0).keys()].map(
                            (column: number) => (
                                <GridField
                                    key={`${row}-${column}`}
                                    type={fields[row][column].type}
                                    algorithmState={fields[row][column].algorithmState}
                                    size={fieldSize}
                                    onClick={() => {
                                        if (onClick) {
                                            onClick(row, column);
                                        }
                                    }}
                                />
                            )
                        )
                    )
                ))}
            </Box>

        </Box>
    );
};
