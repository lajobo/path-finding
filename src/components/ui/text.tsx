import styled from "styled-components";
import {
    space,
    SpaceProps,
    color,
    ColorProps,
    typography,
    TypographyProps,
    layout,
    LayoutProps,
    flexbox,
    FlexboxProps,
    position,
    PositionProps,
    shadow,
    ShadowProps
} from "styled-system";
import React from "react";
import { Theme } from "../../types/themes";

type TextProps =
    SpaceProps &
    ColorProps &
    TypographyProps &
    LayoutProps &
    FlexboxProps &
    PositionProps &
    ShadowProps &
    {
        as?: string,
        theme?: Theme
    };

export const Text: React.FC<TextProps> = styled.div`
    ${(props: TextProps) => `
        font-family: ${props.theme.fonts.default};
        font-size: ${props.theme.fontSizes[2]}px;
        line-height: ${props.theme.lineHeights[2]};
    `}
    ${space}
    ${color}
    ${typography}
    ${layout}
    ${flexbox}
    ${position}
    ${shadow}
`;
