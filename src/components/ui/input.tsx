import styled from "styled-components";
import { border, BorderProps, color, ColorProps, layout, LayoutProps, shadow, ShadowProps, space, SpaceProps } from "styled-system";
import React, { InputHTMLAttributes } from "react";
import { Theme } from "../../types/themes";

type InputProps =
    InputHTMLAttributes<HTMLInputElement> &
    SpaceProps &
    ColorProps &
    LayoutProps &
    BorderProps &
    ShadowProps &
    {
        theme?: Theme
    };

export const Input: React.FC<InputProps> = styled.input`
   ${(props: InputProps) => `font-family: ${props.theme.fonts.default};`}
  ${space}
  ${color}
  ${layout}
  ${border}
  ${shadow}
`;
