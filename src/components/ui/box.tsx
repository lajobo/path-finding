import styled from "styled-components";
import {
    background,
    BackgroundProps,
    border,
    BorderProps,
    color,
    ColorProps,
    flexbox,
    FlexboxProps,
    grid,
    GridProps,
    layout,
    LayoutProps,
    position,
    PositionProps,
    shadow,
    ShadowProps,
    space,
    SpaceProps,
    typography,
    TypographyProps
} from "styled-system";
import React, { DetailedHTMLProps, HTMLAttributes } from "react";

export type BoxProps =
    DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> &
    SpaceProps &
    ColorProps &
    LayoutProps &
    FlexboxProps &
    GridProps &
    BackgroundProps &
    BorderProps &
    PositionProps &
    ShadowProps &
    TypographyProps &
    {
        as?: string;
        ref?: React.Ref<HTMLDivElement>;
        outline?: string;
        cursor?: string;
    };

export const Box: React.FC<BoxProps> = styled.div`
    box-sizing: border-box;
    ${(props: BoxProps) => (props.outline ? `outline: ${props.outline};` : undefined)}
    ${(props: BoxProps) => (props.cursor ? `cursor: ${props.cursor};` : undefined)}
    ${space}
    ${color}
    ${layout}
    ${flexbox}
    ${grid}
    ${background}
    ${border}
    ${position}
    ${shadow}
    ${typography}
`;
