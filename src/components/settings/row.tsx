import React, { PropsWithChildren } from "react";
import { Box } from "../ui/box";
import { Text } from "../ui/text";

interface SettingsRowProps {
    label: string;
}

export const SettingsRow: React.FC<SettingsRowProps> = (props: PropsWithChildren<SettingsRowProps>) => (
    <Box display={"flex"} flexDirection={"row"}>
        <Box flexGrow={1}>
            <Text>
                {props.label}
            </Text>
        </Box>
        <Box>
            {props.children}
        </Box>
    </Box>
);
