import React, { ChangeEvent, useEffect, useState } from "react";
import { Text } from "../ui/text";
import { Box } from "../ui/box";
import { Input } from "../ui/input";
import { SettingsRow } from "./row";
import { InvalidConfigurationMessage } from "../info/invalidConfiguration";

export interface SettingsConfiguration {
    gridWidth: string;
    gridHeight: string;
}

const defaultConfiguration: SettingsConfiguration = {
    gridWidth: "10",
    gridHeight: "10"
};

export interface SettingsProps {
    onConfigurationChange?: (configuration: SettingsConfiguration, isValid: boolean) => void;
}

const checkIsConfigurationValid: (configuration: SettingsConfiguration) => boolean = (configuration: SettingsConfiguration) => {
    let valid: boolean = false;

    try {
        if (configuration.gridWidth && configuration.gridHeight) {
            const width: number = Number(configuration.gridWidth);
            const height: number = Number(configuration.gridHeight);
            if (Number.isInteger(width) && Number.isInteger(height) && width > 0 && height > 0) {
                valid = true;
            }
        }
    } catch (error) { /* do nothing */ }

    return valid;
};

export const Settings: React.FC<SettingsProps> = (props: SettingsProps) => {
    const [configuration, setConfiguration] = useState<SettingsConfiguration>(defaultConfiguration);
    const [isConfigurationValid, setIsConfigurationValid] = useState<boolean>(true);
    const submitConfiguration: () => void = () => {
        const isValid: boolean = checkIsConfigurationValid(configuration);
        setIsConfigurationValid(isValid);

        if (props.onConfigurationChange) {
            props.onConfigurationChange(configuration, isValid);
        }
    };

    useEffect(() => { submitConfiguration(); }, [configuration]);
    useEffect(() => { submitConfiguration(); }, []);

    return (
        <Box width={"100%"}>
            <SettingsRow label={"Grid size"}>
                <Box display={"flex"}>
                    <Input
                        type={"number"}
                        name={"gridWidth"}
                        width={"40px"}
                        value={configuration.gridWidth}
                        onChange={(event: ChangeEvent<HTMLInputElement>) => {
                            setConfiguration({ ...configuration, gridWidth: event.target.value });
                        }}
                    />
                    <Text mx={1} lineHeight={3}>{"x"}</Text>
                    <Input
                        type={"number"}
                        name={"gridHeight"}
                        width={"40px"}
                        value={configuration.gridHeight}
                        onChange={(event: ChangeEvent<HTMLInputElement>) => {
                            setConfiguration({ ...configuration, gridHeight: event.target.value });
                        }}
                    />
                </Box>
            </SettingsRow>
            {!isConfigurationValid && (<InvalidConfigurationMessage mt={1} />)}
        </Box>
    );
};
