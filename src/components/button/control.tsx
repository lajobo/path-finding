import React, { PropsWithChildren } from "react";
import { Box, BoxProps } from "../ui/box";

export const ControlButton: React.FC<PropsWithChildren<BoxProps>> = ({
    children,
    ...boxProps
}: PropsWithChildren<BoxProps>) => (
    <Box
        as={"button"}
        display={"inline-block"}
        flexGrow={1}
        textAlign={"center"}
        background={"none"}
        border={"none"}
        cursor={"pointer"}
        py={1}
        {...boxProps}
    >
        {children}
    </Box>
);
