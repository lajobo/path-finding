import { AlgorithmStates, Field, FieldPosition, Fields, FieldTypes } from "../types/fields";
import {
    areFieldsValid, cloneFields,
    getFieldByPosition,
    getFieldPosition,
    getLowestFFromList,
    getNeighborFieldsPositions,
    getStartField,
    getTargetField,
    isFieldPositionInList,
    isSameFieldPosition,
    removeFieldPositionFromList,
    updateField
} from "./fields";
import { getManhattenDistance } from "./heuristics";
import { Heuristic as HeuristicType } from "../types/heuristics";

export const getAStarPath: (initialFields: Fields) => Promise<Fields[]> = (
    initialFields: Fields
) => new Promise<Fields[]>(((
    resolve: (value: Fields[] | PromiseLike<Fields[]>) => void,
    reject: (reason?: Error) => void
) => {
    if (!areFieldsValid(initialFields)) {
        reject(new Error("Invalid fields"));
    }

    // clone to avoid changes in the given fields which would cause flickering
    const fields: Fields = JSON.parse(JSON.stringify(initialFields));
    const steps: Fields[] = [cloneFields(fields)];
    const startPosition: FieldPosition = getFieldPosition(getStartField(fields));
    const targetPosition: FieldPosition = getFieldPosition(getTargetField(fields));
    let openList: FieldPosition[] = [startPosition];
    const closedList: FieldPosition[] = [];
    let currentFieldPosition: FieldPosition;
    let currentField: Field;
    let neighbors: FieldPosition[];
    let neighborIndex: number;
    let neighbor: Field;
    let neighborH: number;
    let neighborG: number;
    let isNeighborInOpenList: boolean;
    const D: number = 1;
    const heuristic: HeuristicType = getManhattenDistance;

    while (openList.length > 0) {
        // get lowest g = closest to start
        currentFieldPosition = getLowestFFromList(fields, openList);

        // update fields and push the next step
        updateField(fields, currentFieldPosition[0], currentFieldPosition[1], { algorithmState: AlgorithmStates.Current });
        steps.push(cloneFields(fields));

        // you have reached your destination
        if (isSameFieldPosition(currentFieldPosition, targetPosition)) {
            const path: Field[] = [getFieldByPosition(fields, targetPosition)];
            // let lastField: Field = path[0];
            let lastFieldPosition: FieldPosition = targetPosition;

            // update fields and push the next step
            updateField(fields, targetPosition[0], targetPosition[1], { algorithmState: AlgorithmStates.Path });
            steps.push(cloneFields(fields));

            while (!isSameFieldPosition(lastFieldPosition, startPosition)) {
                // build fastest path in reverse
                lastFieldPosition = path[path.length - 1].parentPosition;
                path.push(getFieldByPosition(fields, lastFieldPosition));

                // update fields and push the next step
                updateField(fields, lastFieldPosition[0], lastFieldPosition[1], { algorithmState: AlgorithmStates.Path });
                steps.push(cloneFields(fields));
            }

            resolve(steps);
            return;
        }

        neighbors = getNeighborFieldsPositions(fields, currentFieldPosition);
        currentField = getFieldByPosition(fields, currentFieldPosition);

        for (neighborIndex = 0; neighborIndex < neighbors.length; neighborIndex++) {
            neighbor = getFieldByPosition(fields, neighbors[neighborIndex]);

            // skip field if it was checked before or is a wall
            if (neighbor.type !== FieldTypes.Wall && !isFieldPositionInList(closedList, neighbors[neighborIndex])) {
                // update fields for neighbors but push it after all neighbors were iterated
                updateField(fields, neighbor.row, neighbor.column, { algorithmState: AlgorithmStates.Open });

                // find the best path to the current node by checking its neighbors g
                neighborG = (currentField.g || 0) + D;

                isNeighborInOpenList = isFieldPositionInList(openList, neighbors[neighborIndex]);

                if (!isNeighborInOpenList || neighborG < neighbor.g) {
                    neighborH = !isNeighborInOpenList ? heuristic(neighbors[neighborIndex], targetPosition) : neighbor.h;
                    updateField(
                        fields,
                        neighbor.row,
                        neighbor.column,
                        {
                            parentPosition: currentFieldPosition,
                            g: neighborG,
                            h: neighborH,
                            f: neighborG + neighborH
                        }
                    );

                    if (!isNeighborInOpenList) {
                        openList.push(neighbors[neighborIndex]);
                    }
                }
            }
        }

        // push next step with all neighbor changes as one change
        steps.push(cloneFields(fields));

        openList = removeFieldPositionFromList(openList, currentFieldPosition);
        closedList.push(currentFieldPosition);

        // update fields and push the next step
        updateField(fields, currentField.row, currentField.column, { algorithmState: AlgorithmStates.Closed });
        steps.push(cloneFields(fields));
    }

    // target could not be found, return the steps of the exploration of the fields
    resolve(steps);
}));
