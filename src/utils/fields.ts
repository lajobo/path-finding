import { Field, FieldPosition, FieldRow, Fields, FieldTypes } from "../types/fields";

export const getFieldPosition: (field: Field) => FieldPosition = (field: Field) => [field.row, field.column];

export const getFieldByPosition: (fields: Fields, position: FieldPosition) => Field =
    (fields: Fields, position: FieldPosition) => fields[position[0]][position[1]];

const getFirstFieldOfType: (fields: Fields, type: FieldTypes) => Field | undefined = (
    fields: Fields,
    type: FieldTypes
) => fields.reduce<Field>((found: Field, row: FieldRow) => found || row.find((field: Field) => field.type === type), undefined);

const getAllFieldsOfType: (fields: Fields, type: FieldTypes) => Field[] = (
    fields: Fields,
    type: FieldTypes
) => fields.reduce<Field[]>((found: Field[], row: FieldRow) => [
    ...found,
    ...row.reduce<Field[]>((foundInRow: Field[], field: Field) => [
        ...foundInRow,
        ...(field.type === type ? [field] : [])
    ], [])
], []);

export const getStartField: (fields: Fields) => Field | undefined = (fields: Fields) => getFirstFieldOfType(fields, FieldTypes.Start);

export const getTargetField: (fields: Fields) => Field | undefined = (fields: Fields) => getFirstFieldOfType(fields, FieldTypes.Target);

const hasFieldOfType: (fields: Fields, type: FieldTypes) => boolean = (
    fields: Fields,
    type: FieldTypes
) => getFirstFieldOfType(fields, type) !== undefined;

const hasStartField: (fields: Fields) => boolean = (fields: Fields) => hasFieldOfType(fields, FieldTypes.Start);

const hasTargetField: (fields: Fields) => boolean = (fields: Fields) => hasFieldOfType(fields, FieldTypes.Target);

export const areFieldsValid: (fields: Fields) => boolean = (fields: Fields) => {
    const amountOfStarts: number = getAllFieldsOfType(fields, FieldTypes.Start).length;
    const amountOfTargets: number = getAllFieldsOfType(fields, FieldTypes.Target).length;
    return amountOfStarts === 1 && amountOfTargets === 1;
};

export const isSameFieldPosition: (a: FieldPosition, b: FieldPosition) => boolean =
    (a: FieldPosition, b: FieldPosition) => a[0] === b[0] && a[1] === b[1];

export const isFieldPositionInList: (list: FieldPosition[], fieldPosition: FieldPosition) => boolean = (
    list: FieldPosition[],
    fieldPosition: FieldPosition
) => list.findIndex((listItem: FieldPosition) => isSameFieldPosition(listItem, fieldPosition)) >= 0;

export const removeFieldPositionFromList: (list: FieldPosition[], fieldPosition: FieldPosition) => FieldPosition[] = (
    list: FieldPosition[],
    fieldPosition: FieldPosition
) => list.filter((listFieldPosition: FieldPosition) => !isSameFieldPosition(listFieldPosition, fieldPosition));

interface FieldPositionAndF {
    position: FieldPosition;
    f?: number;
}

export const getLowestFFromList: (fields: Fields, list: FieldPosition[]) => FieldPosition = (fields: Fields, list: FieldPosition[]) => {
    let field: Field;
    return list.reverse().reduce<FieldPositionAndF>(
        (lowest: FieldPositionAndF, fieldPosition: FieldPosition) => {
            field = getFieldByPosition(fields, fieldPosition);
            return !lowest?.f || field.f < lowest.f ? { position: fieldPosition, f: field?.f } : lowest;
        },
        undefined
    ).position;
};

export const cloneFields: (fields: Fields) => Fields = (fields: Fields) => JSON.parse(JSON.stringify(fields));

export const getNeighborFieldsPositions: (fields: Fields, fieldPosition: FieldPosition) => FieldPosition[] = (
    fields: Fields,
    [row, column]: FieldPosition
) => {
    const neighbors: FieldPosition[] = [];

    if (fields[row - 1] && fields[row - 1][column]) {
        neighbors.push([row - 1, column]);
    }
    if (fields[row] && fields[row][column - 1]) {
        neighbors.push([row, column - 1]);
    }
    if (fields[row] && fields[row][column + 1]) {
        neighbors.push([row, column + 1]);
    }
    if (fields[row + 1] && fields[row + 1][column]) {
        neighbors.push([row + 1, column]);
    }

    return neighbors;
};

export const updateFields: (currentFields: Fields, clickedRow: number, clickedColumn: number) => Fields = (
    currentFields: Fields,
    clickedRow: number,
    clickedColumn: number
) => {
    const field: Field = currentFields[clickedRow][clickedColumn];
    let updatedType: FieldTypes;

    switch (field.type) {
        case FieldTypes.Empty:
            updatedType = FieldTypes.Wall;
            break;
        case FieldTypes.Wall:
            if (!hasStartField(currentFields)) {
                updatedType = FieldTypes.Start;
            } else if (!hasTargetField(currentFields)) {
                updatedType = FieldTypes.Target;
            } else {
                updatedType = FieldTypes.Empty;
            }
            break;
        case FieldTypes.Start:
            if (!hasTargetField(currentFields)) {
                updatedType = FieldTypes.Target;
            } else {
                updatedType = FieldTypes.Empty;
            }
            break;
        case FieldTypes.Target:
            updatedType = FieldTypes.Empty;
            break;
    }

    const updatedFields: Fields = cloneFields(currentFields);
    updatedFields[clickedRow][clickedColumn] = {
        ...field,
        row: clickedRow,
        column: clickedColumn,
        type: updatedType
    };

    return updatedFields;
};

/**
 * @param fields The fields reference will be updated!
 * @param row
 * @param column
 * @param data
 */
export const updateField: (fields: Fields, row: number, column: number, data: Partial<Field>) => Field = (
    fields: Fields,
    row: number,
    column: number,
    data: Partial<Field>
) => {
    // eslint-disable-next-line no-param-reassign
    fields[row][column] = {
        ...fields[row][column],
        ...data
    };
    return fields[row][column];
};

export const initializeFields: (rows: number, columns: number) => Fields = (
    rows: number,
    columns: number
) => {
    const buildRow: (rowIndex: number) => FieldRow = (rowIndex: number) => [...Array(columns)].map((columnIndex: number) => ({
        row: rowIndex,
        column: columnIndex,
        type: FieldTypes.Empty
    }));
    const emptyFields: Fields = [...Array(rows)].map((rowIndex: number) => buildRow(rowIndex));
    return emptyFields;
};

export const updateFieldsSize: (currentFields: Fields, rows: number, columns: number) => Fields = (
    currentFields: Fields,
    rows: number,
    columns: number
) => {
    let updatedFields: Fields = initializeFields(rows, columns);

    if (currentFields) {
        updatedFields = updatedFields.map(
            (row: FieldRow, rowIndex: number) => row.map(
                (field: Field, columnIndex: number) => ({
                    row: rowIndex,
                    column: columnIndex,
                    type: currentFields && currentFields[rowIndex] && currentFields[rowIndex][columnIndex]
                        ? currentFields[rowIndex][columnIndex].type
                        : field.type
                })
            )
        );
    }

    return updatedFields;
};

export const resetPathFinding: (currentFields: Fields) => Fields = (
    currentFields: Fields
) => currentFields.map((row: FieldRow) => row.map((field: Field) => ({
    row: field.row,
    column: field.column,
    type: field.type
})));
