import { FieldPosition } from "../types/fields";
import { Heuristic } from "../types/heuristics";

/**
 * @link http://theory.stanford.edu/~amitp/GameProgramming/Heuristics.html#manhattan-distance
 */
export const getManhattenDistance: Heuristic = (
    [currentX, currentY]: FieldPosition,
    [targetX, targetY]: FieldPosition,
    D: number = 1
) => {
    const distanceX: number = Math.abs(currentX - targetX);
    const distanceY: number = Math.abs(currentY - targetY);
    return D * (distanceX + distanceY);
};
