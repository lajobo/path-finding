import { useEffect, useState } from "react";
import { OnControlClick, OnFieldClick, PathFindingAlgorithm } from "../types/pathFinding";
import { Fields } from "../types/fields";
import { resetPathFinding, updateFields, updateFieldsSize } from "../utils/fields";
import { getAStarPath } from "../utils/pathfinding";

export const useAStar: PathFindingAlgorithm = (height: number, width: number) => {
    const [fields, setFields] = useState<Fields>(undefined);
    const [isAutoActive, setAutoActive] = useState<boolean>(false);
    const [algorithmSteps, setAlgorithmSteps] = useState<Fields[]>(undefined);
    const [algorithmStepIndex, setAlgorithmStepIndex] = useState<number>();
    const [fieldsInvalid, setFieldsInvalid] = useState<boolean>(false);
    const [timeoutId, setTimeoutId] = useState<NodeJS.Timeout>(undefined);

    const getAlgorithmSteps: () => Promise<number> = async () => {
        if (!algorithmSteps) {
            const stepLength: number = await getAStarPath(fields)
                .then((steps: Fields[]) => {
                    setFieldsInvalid(false);
                    setAlgorithmSteps(steps);
                    setAlgorithmStepIndex(0);
                    return steps.length;
                })
                .catch(() => {
                    setFieldsInvalid(true);
                    return 0;
                });

            return stepLength;
        }

        return algorithmSteps.length;
    };

    const reset: () => void = () => {
        setFieldsInvalid(false);
        setAlgorithmSteps(undefined);
        setAlgorithmStepIndex(0);
        clearTimeout(timeoutId);
    };

    const handleAuto: () => void = () => {
        if (isAutoActive) {
            if (algorithmSteps && (algorithmSteps.length - 1) > algorithmStepIndex) {
                // the auto handling should just check, if the auto mode is active and if so update the algorithmStepIndex delayed.
                // everything else will be handled by the algorithmStepIndex useEffect and the so created loop.
                setTimeoutId(setTimeout(() => {
                    setAlgorithmStepIndex(algorithmStepIndex + 1);
                }, 100));
            } else {
                // turn off auto mode when the target was reached
                setAutoActive(false);
            }
        }
    };

    const updateFieldSize: () => void = () => {
        setFields(
            resetPathFinding(
                updateFieldsSize(fields, height, width)
            )
        );
        reset();
    };

    const onFieldClick: OnFieldClick = (row: number, column: number) => {
        setFields(
            resetPathFinding(
                updateFields(fields, row, column)
            )
        );
        reset();
    };

    const onFirstClick: OnControlClick = () => {
        // no need to handle the promise since the state handling will update everything automatically once the steps are calculated
        getAlgorithmSteps().then().catch();

        setAlgorithmStepIndex(0);
    };

    const onPrevClick: OnControlClick = async () => {
        // no need to handle the promise since the state handling will update everything automatically once the steps are calculated
        getAlgorithmSteps().then().catch();

        if (algorithmStepIndex > 0) {
            setAlgorithmStepIndex(algorithmStepIndex - 1);
        }
    };

    const onPlayClick: OnControlClick = () => {
        setAutoActive(true);
    };

    const onPauseClick: OnControlClick = () => {
        setAutoActive(false);
    };

    const onNextClick: OnControlClick = async () => {
        const stepsLength: number = await getAlgorithmSteps();

        if (!algorithmSteps) {
            if (stepsLength > 0) {
                setAlgorithmStepIndex(1);
            }
        } else if ((algorithmSteps.length - 1) > algorithmStepIndex) {
            setAlgorithmStepIndex(algorithmStepIndex + 1);
        }
    };

    const onLastClick: OnControlClick = async () => {
        const stepsLength: number = await getAlgorithmSteps();
        setAlgorithmStepIndex(stepsLength - 1);
    };

    useEffect(() => {
        updateFieldSize();
    }, [height, width]);

    useEffect(() => {
        if (isAutoActive) {
            if (algorithmSteps) {
                // algorithmSteps are already filled which means, that the path finding was already initiated, so that it could be continued
                handleAuto();
            } else {
                // algorithmSteps are empty, therefore the path finding need to be initiated first. all upcoming steps will be handled by the
                // algorithmSteps and algorithmStepsIndex useEffect.
                getAlgorithmSteps().then().catch();
            }
        } else {
            // clear the timeout to not get 1 unnecessary further step executed
            clearTimeout(timeoutId);
            setTimeoutId(undefined);
        }

        // clear timeout on unmount
        return () => {
            clearTimeout(timeoutId);
        };
    }, [isAutoActive]);

    useEffect(() => {
        // check if the auto mode was just activated
        handleAuto();
    }, [algorithmSteps]);

    useEffect(() => {
        // check if the auto mode is active and should continue the path finding
        handleAuto();
    }, [algorithmStepIndex]);

    return {
        fields: algorithmStepIndex && algorithmSteps ? algorithmSteps[algorithmStepIndex] : fields,
        fieldsInvalid,
        hasNextStep: !fieldsInvalid && (!algorithmSteps || (algorithmSteps.length - 1) > algorithmStepIndex),
        hasPrevStep: !fieldsInvalid && algorithmStepIndex > 0,
        isAutoActive,
        onFieldClick,
        onFirstClick,
        onPrevClick,
        onPlayClick,
        onPauseClick,
        onNextClick,
        onLastClick
    };
};
