import { BrowserRouter } from "react-router-dom";
import React from "react";
import { hydrate } from "react-dom";
import { App } from "./components/app/app";

declare let module: any; // eslint-disable-line @typescript-eslint/no-explicit-any

hydrate(
    <BrowserRouter>
        <App />
    </BrowserRouter>,
    document.getElementById("root")
);

if (module.hot) {
    module.hot.accept();
}
