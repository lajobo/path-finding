export enum FieldTypes {
    Empty = "empty",
    Wall = "wall",
    Start = "start",
    Target = "target"
}

export enum AlgorithmStates {
    Current = "current",
    Open = "open",
    Closed = "closed",
    Path = "path"
}

export type FieldPosition = [number, number]; // [row, column]

export interface Field {
    row: number;
    column: number;
    type: FieldTypes;
    parentPosition?: FieldPosition;
    algorithmState?: AlgorithmStates;
    /**
     * the cost of reaching that field from the start
     */
    g?: number;
    /**
     * the estimated cost of reaching the target from that field
     */
    h?: number;
    /**
     * the total cost from start to target going over that field
     */
    f?: number;
}

export type FieldRow = Field[];

export type Fields = FieldRow[];
