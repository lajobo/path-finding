import { FieldPosition } from "./fields";

export type Heuristic = (
    currentPosition: FieldPosition,
    targetPosition: FieldPosition,
    D?: number
) => number;
