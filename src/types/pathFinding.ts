import { Fields } from "./fields";

export type OnFieldClick = (row: number, column: number) => void;

export type OnControlClick = () => void;

export interface PathFinding {
    fields: Fields;
    fieldsInvalid: boolean;
    hasNextStep: boolean;
    hasPrevStep: boolean;
    isAutoActive: boolean;
    onFieldClick: OnFieldClick;
    onFirstClick: OnControlClick;
    onPrevClick: OnControlClick;
    onPlayClick: OnControlClick;
    onPauseClick: OnControlClick;
    onNextClick: OnControlClick;
    onLastClick: OnControlClick;
}

export type PathFindingAlgorithm = (height: number, width: number) => PathFinding;
