export interface Theme {
    space: [number, number, number, number, number, number, number, number];
    fontSizes: [number, number, number, number, number, number, number, number];
    lineHeights: [string, string, string, string, string, string, string, string];
    breakpoints: [string, string, string, string];
    colors: {
        grey: {
            dark: string;
            medium: string;
            bright: string;
        };
        error: string;
        fields: {
            empty: string;
            wall: string;
            start: string;
            target: string;
            current: string;
            open: string;
            closed: string;
            path: string;
        };
    };
    fonts: {
        default: string;
        headings: string;
    };
}
