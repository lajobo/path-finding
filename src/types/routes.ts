import * as core from "express";

export interface RequestWithCookies extends core.Request {
    universalCookies: {
        get: (name: string) => string;
    };
}

export type Route = (req: RequestWithCookies, res: core.Response) => void;
