import express, { Application } from "express";
import compression from "compression";
import { asyncMiddleware } from "./middlewares/async";
import { router } from "./routes";
import { accessLogMiddleware } from "./middlewares/accessLog";
import { errorHandlerMiddleware } from "./middlewares/errorHandler";

const app: Application = express();

app.set("trust proxy", true);

app.use(compression());
app.use(express.json());
app.use(accessLogMiddleware);
app.disable("x-powered-by");
app.use(express.static(process.env.RAZZLE_PUBLIC_DIR));
app.use(asyncMiddleware(router));
app.use(errorHandlerMiddleware);

export default app;
