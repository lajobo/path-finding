import { Response } from "express";
import dateFormat from "dateformat";
import { RequestWithCookies } from "../../types/routes";

const getDurationInMilliseconds: (start: [number, number]) => number = (start: [number, number]) => {
    const NS_PER_SEC: number = 1e9;
    const NS_TO_MS: number = 1e6;
    const diff: [number, number] = process.hrtime(start);

    return (diff[0] * NS_PER_SEC + diff[1]) / NS_TO_MS;
};

const log: (res: Response, req: RequestWithCookies, start: [number, number]) => void = (
    res: Response,
    req: RequestWithCookies,
    start: [number, number]
) => {
    const durationInMilliseconds: number = getDurationInMilliseconds(start);

    // eslint-disable-next-line no-console
    console.log([
        dateFormat(new Date(), "yyyy-mm-dd hh:MM:ss"),
        res.statusCode,
        durationInMilliseconds.toFixed(2).toLocaleString().padStart(6, " "),
        req.url
    ].join(" - "));
};

export const accessLogMiddleware: (req: RequestWithCookies, res: Response, next: () => void) => void = (
    req: RequestWithCookies,
    res: Response,
    next: () => void
) => {
    const start: [number, number] = process.hrtime();

    res.on("finish", () => log(res, req, start));
    res.on("close", () => log(res, req, start));

    next();
};
