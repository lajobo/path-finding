import * as core from "express";

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const errorHandlerMiddleware: (err: any, req: core.Request, res: core.Response, next: () => void) => void = (
    err: any, // eslint-disable-line @typescript-eslint/no-explicit-any
    req: core.Request,
    res: core.Response,
    next: () => void
) => {
    // eslint-disable-next-line no-console
    console.error("Error", err);

    next();
};
