import { Router } from "express";
import { healthRoute } from "./health";
import { appRoute } from "./app";

export const router: Router = Router();

router.get("/health", healthRoute);
router.get("/*", appRoute);
