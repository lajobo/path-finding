import * as core from "express";
import React from "react";
import { StaticRouter } from "react-router-dom";
import { renderToString } from "react-dom/server";
import { renderDocumentTemplate } from "./app/documentTemplate";
import { App } from "../../components/app/app";
import { Route } from "../../types/routes";

// eslint-disable-next-line @typescript-eslint/no-explicit-any,import/no-dynamic-require
const assets: any = require(process.env.RAZZLE_ASSETS_MANIFEST);

export const appRoute: Route = (req: core.Request, res: core.Response) => {
    const context: any = {}; // eslint-disable-line @typescript-eslint/no-explicit-any
    const markup: string = renderToString(
        <StaticRouter context={context} location={req.url}>
            <App />
        </StaticRouter>
    );

    if (context.url) {
        res.redirect(context.url);
    } else {
        res.status(200).send(renderDocumentTemplate(assets, markup));
    }
};
