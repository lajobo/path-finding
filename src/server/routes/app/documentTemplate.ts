const cssLinksFromAssets: (
    assets: unknown,
    entrypoint: string
) => string = (
    assets: unknown,
    entrypoint: string
) => (
    assets[entrypoint]
        ? assets[entrypoint].css
            ? assets[entrypoint].css.map((asset) => `<link rel="stylesheet" href="${asset}">`).join("")
            : ""
        : ""
);

const jsScriptTagsFromAssets: (
    assets: unknown,
    entrypoint: string,
    extra?: string
) => string = (
    assets: unknown,
    entrypoint: string,
    extra: string = ""
) => (
    assets[entrypoint]
        ? assets[entrypoint].js
            ? assets[entrypoint].js.map((asset) => `<script src="${asset}"${extra}></script>`).join("")
            : ""
        : ""
);

export const renderDocumentTemplate: (
    assets: any, // eslint-disable-line @typescript-eslint/no-explicit-any
    markup: string
) => string = (
    assets: any, // eslint-disable-line @typescript-eslint/no-explicit-any
    markup: string
) => `<!doctype html>
    <html lang="">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta charset="utf-8" />
        <title>Path finding</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        ${cssLinksFromAssets(assets, "client")}
        <style>html, body, #root { height: 100%; }</style>
        <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
    </head>
    <body>
        <div id="root">${markup}</div>
        ${jsScriptTagsFromAssets(assets, "client", " defer crossorigin")}
    </body>
</html>`;
