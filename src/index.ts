/* eslint-disable no-console,@typescript-eslint/no-explicit-any */
import express from "express";
import App from "./server";

declare let module: any;

let app: any = App;

if (module.hot) {
    module.hot.accept("./server", () => {
        console.log("🔁  HMR Reloading `./server`...");
        try {
            app = App;
        } catch (error) {
            console.error(error);
        }
    });
    console.info("✅  Server-side HMR Enabled!");
}

const port: number = Number(process.env.PORT) || 3000;

export default express()
    .use((req: any, res: any) => app.handle(req, res))
    // @ts-ignore
    .listen(port, (err) => {
        if (err) {
            console.error(err);
            return;
        }
        console.log(`> Started on port ${port}`);
    });
