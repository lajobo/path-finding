module.exports = {
    extends: ["airbnb-typescript"],
    parserOptions: { project: "./tsconfig.json" },
    settings: { "import/resolver": { node: { paths: ["src"] } } },
    rules: {
        "comma-dangle": ["error", {
            arrays: "never",
            objects: "never",
            imports: "never",
            exports: "never",
            functions: "never"
        }],
        "default-case": "off",
        "import/prefer-default-export": "off",
        "max-len": ["warn", { code: 150 }],
        "no-plusplus": "off",
        "no-nested-ternary": "off",
        "operator-linebreak": ["error", "after", { overrides: { "?": "before", ":": "before" } }],
        "jsx-a11y/click-events-have-key-events": "off",
        "jsx-a11y/no-noninteractive-element-interactions": "off",
        "object-curly-newline": ["error", {
            ObjectExpression: { multiline: true, minProperties: 4 },
            ObjectPattern: { multiline: true },
            ImportDeclaration: { multiline: true },
            ExportDeclaration: { multiline: true, minProperties: 3 }
        }],
        "react/destructuring-assignment": "off",
        "react/jsx-boolean-value": ["warn", "never"],
        "react/jsx-curly-brace-presence": ["error", { props: "always", children: "always" }],
        "react/jsx-indent": "off",
        "react/jsx-indent-props": ["error", 4],
        "react/jsx-props-no-spreading": "off",
        "react/prop-types": "off",
        "react/require-default-props": "off",
        semi: "off",
        "@typescript-eslint/comma-dangle": "off",
        "@typescript-eslint/comma-spacing": "off",
        "@typescript-eslint/indent": ["error", 4],
        "@typescript-eslint/no-explicit-any": ["error"],
        "@typescript-eslint/quotes": ["error", "double"],
        "@typescript-eslint/semi": "error",
        "@typescript-eslint/typedef": ["error", { variableDeclaration: true }],
        "jsx-a11y/no-static-element-interactions": "off"
    }
};
